/*
//Filename: libBBB.h
//Version : 0.11
//
*/


#ifndef _libBBB_H_
#define _libBBB_H_

//Includes
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include <termios.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/time.h>

//Type definitions
typedef struct {
      struct termios u;
}UART;

//Definitions
#define OUTPUT	"out"
#define INPUT	"in"
#define HIGH    1
#define LOW     0
#define ON      1
#define OFF     0

#define USR1	"usr1"
#define USR2	"usr2"
#define USR3	"usr3"
#define A0	"/AIN0"
#define A1	"/AIN1"
#define A2	"/AIN2"
#define A3	"/AIN3"
#define A4	"/AIN4"
#define A5	"/AIN5"
#define A6	"/AIN6"
#define A7	"/AIN7"
#define HELPNUM 15
#define TEST	0

// PWM channels by pin name
#define P9_14 0
#define P9_16 1
#define P8_19 2
#define P8_13 3
#define P9_21 4
#define P9_22 5
#define P9_42 6

// channel mapping for ROB 550 interface board
#define SV1 "P9_14"
#define SV2 "P9_16"
#define SV3 "P8_19"
#define SV4 "P8_13"
#define SV5 "P9_42"

#define SV1H 16
#define SV2H 17
#define SV3H 17
#define SV4H 19
#define SV5H 20


//Device Tree Overlay
int addOverlay(char *dtb, char *overname);

//USR Prototypes
int setUsrLedValue(char* led, int value);

//GPIO Prototypes
int initPin(int pinnum);
int setPinDirection(int pinnum, char* dir);
int setPinValue(int pinnum, int value);
int getPinValue(int pinnum);

//PWM Prototypes
int initPWM(char* pin);
int setPWMPeriod(int helpnum, char* pin, int period);
int setPWMPolarity(int helpnum, char* pin, int polarity);
int setPWMDuty(int helpnum, char* pin, int duty);
int setPWMOnOff(int helpnum, char* pin, int run);

/*
//UART Prototypes
int initUART(int mgrnum, char* uartnum);
void closeUART(int fd);
int configUART(UART u, int property, char* value);
int txUART(int uart, unsigned char data);
unsigned char rxUART(int uart);
int strUART(int uart, char* buf);
*/

//I2C Prototypes
int initI2C(int modnum, int addr);
void closeI2C(int device);
int writeByteI2C(int device, unsigned char *data);
int writeBufferI2C(int device, unsigned char *buf, int len);
int readByteI2C(int device, unsigned char *data);
int readBufferI2C(int device, int numbytes, unsigned char *buf);

//SPI Prototypes
int initSPI(int modnum);
void closeSPI(int device);
int writeByteSPI(int device,unsigned char *data);
int writeBufferSPI(int device, unsigned char *buf, int len);
int readByteSPI(int device, unsigned char *data);
int readBufferSPI(int device, int numbytes, unsigned char *buf);

//ADC Prototypes
int initADC(void);
int readADC(int helpnum, char* ach);

//Time Prototypes
int64_t utime_now (void);

#endif
